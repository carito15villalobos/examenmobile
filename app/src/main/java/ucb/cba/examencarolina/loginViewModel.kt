package ucb.cba.examencarolina

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class loginViewModel(val loginRepository: loginRepository): ViewModel() {
    val model: LiveData<UiModel>
        get()=_model
    private val _model= MutableLiveData<UiModel>()

    sealed class UiModel{
        class Login(val success:Boolean): UiModel()
        object Loading:UiModel()

    }
    fun doLogin(name: String, lastName: String, email:String){
        _model.value=UiModel.Loading
        val runnable= Runnable {
            _model.value= UiModel.Login(loginRepository.saveUSer(name, lastName, email))
        }
        Handler().postDelayed(runnable,3000)
    }
}