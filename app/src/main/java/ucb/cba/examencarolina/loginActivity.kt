package ucb.cba.examencarolina

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_login.*

class loginActivity : AppCompatActivity() {

    lateinit var loginViewModel: loginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginViewModel=loginViewModel(loginRepository())

        loginViewModel.model.observe(this, Observer  (::updateUi))
        register_btn.setOnClickListener{
            loginViewModel.doLogin(etName.text.toString(),etLastName.text.toString(),etEmail.text.toString())
        }
    }
    private fun updateUi(model: loginViewModel.UiModel?) {
        progress_bar.visibility=if (model is loginViewModel.UiModel.Loading) View.VISIBLE else View.GONE
        when (model ){
            is loginViewModel.UiModel.Login->validarLogin(model.success)
        }

    }
    private fun validarLogin(resp:Boolean){
        if(resp ){
            Toast.makeText(this,"resgister exitoso", Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(this,"register no exitoso", Toast.LENGTH_LONG).show()
        }

    }
}
